<?php

include("../connection/config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: list-siswa.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM calon_siswa WHERE id=$id";
$query = mysqli_query($db, $sql);
$siswa = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

?>


<!DOCTYPE html>
<html>
<head>
    <title>Formulir Edit Siswa | SMK Latihan</title>
</head>

<body>
    <header>
        <h3>Formulir Edit Siswa</h3>
    </header>

    <form action="proses-edit.php" method="POST">

        <fieldset>
            <input type="hidden" name="id" value="<?php echo $siswa['id'] ?>" />
			<table>
				<tr>
					<td><label for="nama">Nama: </label></td>
					<td><input type="text" name="nama" placeholder="Nama Lengkap" value="<?php echo $siswa['nama'] ?>" /></td>
				</tr>
				<tr>
					<td><label for="alamat">Alamat: </label></td>
					<td><textarea name="alamat"><?php echo $siswa['alamat'] ?></textarea></td>
				</tr>
				<tr>
					<td><label for="jenis_kelamin">Jenis Kelamin: </label></td>
					<?php $jk = $siswa['jenis_kelamin']; ?>
					<td><label><input type="radio" name="jenis_kelamin" value="Laki-Laki" <?php echo ($jk == 'Laki-Laki') ? "checked": "" ?>> Laki-laki</label><br>
					<label><input type="radio" name="jenis_kelamin" value="Perempuan" <?php echo ($jk == 'Perempuan') ? "checked": "" ?>> Perempuan</label></td>
				</tr>
				<tr>
					<td><label for="agama">Agama: </label>
					<?php $agama = $siswa['agama']; ?>
					<td><select name="agama">
						<option <?php echo ($agama == 'Islam') ? "selected": "" ?>>Islam</option>
						<option <?php echo ($agama == 'Kristen') ? "selected": "" ?>>Kristen</option>
						<option <?php echo ($agama == 'Hindu') ? "selected": "" ?>>Hindu</option>
						<option <?php echo ($agama == 'Budha') ? "selected": "" ?>>Budha</option>
						<option <?php echo ($agama == 'Atheis') ? "selected": "" ?>>Atheis</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="sekolah_asal">Sekolah Asal: </label></td>
					<td><input type="text" name="sekolah_asal" placeholder="Nama Sekolah" value="<?php echo $siswa['sekolah_asal'] ?>" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Simpan" name="simpan" /></td>
				</tr>
			</table>
        </fieldset>


    </form>

    </body>
</html>