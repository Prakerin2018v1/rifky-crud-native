<?php

	include("../connection/config.php");
	
	if( isset($_GET['id']) ){

		// ambil id dari query string
		$id = $_GET['id'];

		// buat query hapus
		$sql = "DELETE FROM calon_siswa WHERE id=$id;";
		$query = mysqli_query($db, $sql);
		
		// untuk mereset auto increment
		$sql1 = "ALTER TABLE calon_siswa AUTO_INCREMENT = 1;";
		$query1 = mysqli_query($db, $sql1);
		
		// apakah query hapus berhasil?
		if( $query ){
			header('Location: list-siswa.php');
		} else {
			die("gagal menghapus...");
		}

	} else {
		die("akses dilarang...");
	}

?>